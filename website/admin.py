from django.contrib import admin
from .models import MarkdownPage
from markdownx.admin import MarkdownxModelAdmin

admin.site.register(MarkdownPage, MarkdownxModelAdmin)
