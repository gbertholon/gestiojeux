import markdown
import re


class NbspPreprocessor(markdown.preprocessors.Preprocessor):
    """Replace regular spaces with non-breaking spaces within a text around relevant
    symbols"""

    NBSP_BEFORE = [":", "!", "?", "»", ":", ";", "—", "€"]
    NBSP_AFTER = ["«", "—"]

    def run(self, lines):
        text = "\n".join(lines)
        re_before = re.compile("(?: *\n *| +)([{}])".format("".join(self.NBSP_BEFORE)))
        re_after = re.compile("([{}])(?: +| *\n *)".format("".join(self.NBSP_AFTER)))

        text = re_before.sub(r"&nbsp;\1", text)
        text = re_after.sub(r"\1&nbsp;", text)
        return text.split("\n")


class MainSiteMarkdownExtension(markdown.extensions.Extension):
    def extendMarkdown(self, md):
        md.preprocessors.register(NbspPreprocessor(md), "nbsp", 10)


def makeExtension(**kwargs):
    return MainSiteMarkdownExtension(**kwargs)
