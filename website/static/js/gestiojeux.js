const MOBILE_NAV_PAGE_SIZE = 500; // px
const MOBILE_NAV_TOGGLE_TIME = 300; // msec
const TEXTAREA_MIN_SIZE = 120; // px

function toggle_mobile_nav() {
    $("nav").slideToggle(MOBILE_NAV_TOGGLE_TIME);
}

function check_mobile_nav_window_size() {
    if(window.innerWidth > MOBILE_NAV_PAGE_SIZE) {
        $("nav").show(0);
        $("#mobile_nav_trigger").hide(0);
    }
    else {
        $("nav").hide(0);
        $("#mobile_nav_trigger").show(0);
    }
}

$(window).resize(check_mobile_nav_window_size);
check_mobile_nav_window_size();

$("textarea").each(function () {
    this.style.height = Math.max(this.scrollHeight, TEXTAREA_MIN_SIZE) + "px";
    this.style.resize = "none";
    this.style.overflowY = "hidden";
}).on("input", function () {
    this.style.height = "auto";
    this.style.height = Math.max(this.scrollHeight, TEXTAREA_MIN_SIZE) + "px";
});
