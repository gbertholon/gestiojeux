import django_tables2 as tables
from django.utils.html import format_html
from django.urls import reverse
from .models import GameLoan

class LoanTable(tables.Table):
    next_pattern = "inventory:all_loans"

    class Meta:
        model = GameLoan
        sequence = ("lent_object", "borrow_date", "return_date", "mail")
        exclude = ("id",)
    slug = tables.Column(verbose_name="Actions", orderable=False)

    def render_lent_object(self, value, record):
        return format_html("<a href='{}'>{}</a>",
            reverse("inventory:game", args=[record.lent_object.slug]), value)

    def render_slug(self, value, record):
        res = ""
        if record.return_date == None:
            res = format_html("<a class='button' href='{}?next={}'>Rendre le jeu</a>",
                reverse("inventory:return_game", args=[value]),
                reverse(self.next_pattern))
        return res
    

class OngoingLoansTable(LoanTable):
    next_pattern = "inventory:ongoing_loans"

    class Meta(LoanTable.Meta):
        exclude = ("return_date", "mail", "id")

