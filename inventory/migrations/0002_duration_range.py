# Generated by Django 3.1.5 on 2021-01-28 23:15

from django.db import migrations, models


def extract_duration_range(apps, schema_editor):
    Game = apps.get_model("inventory", "Game")
    for game in Game.objects.all():
        sep = None
        if game.duration.count("-") == 1:
            sep = "-"
        elif game.duration.count("à") == 1:
            sep = "à"

        if sep:
            duration_split = game.duration.split(sep)
            game.duration_min = int(duration_split[0])
            game.duration_max = int(duration_split[1])
        else:
            single_duration = int(game.duration)
            game.duration_min = single_duration
            game.duration_max = single_duration

        game.duration = ""
        game.save()


class Migration(migrations.Migration):

    dependencies = [
        ("inventory", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="game",
            name="duration_max",
            field=models.PositiveSmallIntegerField(
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="game",
            name="duration_min",
            field=models.PositiveSmallIntegerField(
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="game",
            name="duration",
            field=models.CharField(
                blank=True,
                help_text="Affichage personnalisé pour la durée de la partie",
                max_length=256,
                verbose_name="durée de partie",
            ),
        ),
        migrations.RunPython(extract_duration_range),
        migrations.AlterField(
            model_name="game",
            name="duration_max",
            field=models.PositiveSmallIntegerField(
                blank=True,
                help_text="En minutes, telle qu'indiquée par l'éditeur, identique à la durée minimale si laissée vide",
                verbose_name="durée de partie maximale",
            ),
        ),
        migrations.AlterField(
            model_name="game",
            name="duration_min",
            field=models.PositiveSmallIntegerField(
                help_text="En minutes, telle qu'indiquée par l'éditeur",
                verbose_name="durée de partie minimale",
            ),
        ),
    ]
