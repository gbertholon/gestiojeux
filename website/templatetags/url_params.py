from django import template

register = template.Library()


@register.simple_tag()
def replace_url_param(request, **kwargs):
    url_params = request.GET.copy()
    for k, v in kwargs.items():
        if v is not None:
            url_params[k] = v
        else:
            del url_params[k]
    return url_params.urlencode()
