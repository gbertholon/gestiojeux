from django.views.generic import DetailView
from django.utils.safestring import mark_safe
from markdownx.utils import markdownify
from .models import MarkdownPage


class MarkdownPageView(DetailView):
    model = MarkdownPage
    template_name = "website/markdown_page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["markdown_body"] = mark_safe(markdownify(self.object.content))
        return context
