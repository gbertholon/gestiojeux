from django import forms
from .models import Suggestion


class SuggestionForm(forms.ModelForm):
    error_css_class = "errorfield"
    required_css_class = "requiredfield"

    class Meta:
        model = Suggestion
        fields = [
            "title",
            "price",
            "buy_link",
            "nb_player_min",
            "nb_player_max",
            "player_range_precisions",
            "duration_min",
            "duration_max",
            "duration_precisions",
            "category",
            "tags",
            "game_designer",
            "illustrator",
            "editor",
            "image",
            "description",
        ]


class AddSuggestionForm(SuggestionForm):
    comment = forms.CharField(widget=forms.Textarea, label="Commentaire personnel")
