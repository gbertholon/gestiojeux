from django.db import models
from django.urls import reverse
from markdownx.models import MarkdownxField


class MarkdownPage(models.Model):
    slug = models.SlugField(
        blank=True,
        unique=True,
        verbose_name="Adresse de la page",
        help_text="Identifiant de la page qui se voit dans l'URL. Ne doit pas collisionner avec une page existante. Laisser vide pour la page d'accueil, requis sinon.",
    )
    content = MarkdownxField(verbose_name="Contenu")

    class Meta:
        verbose_name = "page Markdown"
        verbose_name_plural = "pages Markdown"
        ordering = ["slug"]

    def __str__(self):
        return self.slug or "Page d'accueil"

    def get_absolute_url(self):
        if self.slug:
            return reverse("website:md_page", args=[self.slug])
        else:
            return reverse("website:home")
