from django.contrib import admin


class CommentAdmin(admin.ModelAdmin):
    list_display = ("created_on", "author", "commented_object", "text")
