from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible


@deconstructible
class MaxFileSizeValidator:
    """
    Reject all the files stored in a FileField (or ImageField) that are too heavy.
    The size limit is given in Kio.
    """

    def __init__(self, size_limit):
        self.size_limit = size_limit

    def __call__(self, value):
        if value.size > self.size_limit * 1024:
            raise ValidationError(
                "La taille du fichier doit être inférieure à {} Kio".format(
                    self.size_limit
                ),
                code="file_too_big",
            )

    def __eq__(self, other):
        return (
            isinstance(other, MaxFileSizeValidator)
            and self.size_limit == other.size_limit
        )
