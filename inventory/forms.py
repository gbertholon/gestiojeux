from loans.forms import BorrowForm

class BorrowGameForm(BorrowForm):
    error_css_class = "errorfield"
    required_css_class = "requiredfield"
