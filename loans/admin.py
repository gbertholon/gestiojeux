from django.contrib import admin

class LoanAdmin(admin.ModelAdmin):
    list_display = ("lent_object", "borrow_date", "return_date")
    ordering = ("-borrow_date",)
    list_filter = ("return_date", "borrow_date")
