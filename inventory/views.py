from django.views.generic import TemplateView, ListView, DetailView
from django.contrib.auth.mixins import PermissionRequiredMixin
from haystack.generic_views import SearchView
from haystack.forms import SearchForm
from haystack.query import SearchQuerySet
from django_tables2.views import SingleTableView
from comments.views import AddCommentView, ModifyCommentView
from loans.views import BorrowView, ReturnView, DetailLoanView
from .models import Category, Tag, Game, GameComment, GameLoan
from .forms import BorrowGameForm
from .tables import LoanTable, OngoingLoansTable


class InventoryView(TemplateView):
    template_name = "inventory/inventory.html"


class CategoryListView(ListView):
    model = Category
    template_name = "inventory/category_list.html"
    paginate_by = 20


class CategoryView(DetailView):
    model = Category
    template_name = "inventory/category.html"


class TagListView(ListView):
    model = Tag
    template_name = "inventory/tag_list.html"
    paginate_by = 20


class TagView(DetailView):
    model = Tag
    template_name = "inventory/tag.html"


class GameListView(ListView):
    model = Game
    template_name = "inventory/game_list.html"
    paginate_by = 20


class GameView(DetailLoanView):
    model = Game
    loan_model = GameLoan
    template_name = "inventory/game.html"


class InventorySearchView(SearchView):
    form_class = SearchForm
    template_name = "inventory/search.html"
    paginate_by = 20

    def get_queryset(self):
        return SearchQuerySet().models(Category, Tag, Game)


class AddGameCommentView(AddCommentView):
    model = Game
    comment_model = GameComment
    pattern_name = "inventory:game"


class ModifyGameCommentView(ModifyCommentView):
    model = Game
    comment_model = GameComment
    template_name = "inventory/game.html"
    success_pattern_name = "inventory:game"


class BorrowGameView(BorrowView):
    model = Game
    loan_model = GameLoan
    template_name = "inventory/loans/borrow.html"
    form_class = BorrowGameForm
    success_pattern_name = "inventory:game_loan"


class ReturnGameView(ReturnView):
    model = GameLoan
    pattern_name = "inventory:game_loan"


class GameLoanView(DetailLoanView):
    model = Game
    loan_model = GameLoan
    template_name = "inventory/loans/game_loan.html"


class OngoingLoansView(SingleTableView):
    queryset = GameLoan.ongoing_loans()
    table_class = OngoingLoansTable
    template_name = "inventory/loans/ongoing.html"


class DetailLoanView(PermissionRequiredMixin, SingleTableView):
    permission_required = "inventory.can_see_loan_details"
    model = GameLoan
    table_class = LoanTable
    template_name = "inventory/loans/loans_table.html"

