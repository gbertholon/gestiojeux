from django.forms import ModelForm, ValidationError
from .models import User


class AccountSettingsForm(ModelForm):
    error_css_class = "errorfield"

    class Meta:
        model = User
        fields = ["public_name"]

    def clean_public_name(self):
        public_name = self.cleaned_data["public_name"]
        public_name = public_name.strip()
        if (
            User.objects.filter(public_name=public_name)
            .exclude(pk=self.instance.pk)
            .exists()
        ):
            raise ValidationError("Un autre compte utilise déjà ce nom ou ce pseudo.")
        return public_name
