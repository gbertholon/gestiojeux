from django.urls import path
from .views import (
    InventoryView,
    CategoryListView,
    CategoryView,
    TagListView,
    TagView,
    GameListView,
    GameView,
    AddGameCommentView,
    ModifyGameCommentView,
    InventorySearchView,
    GameLoanView,
    BorrowGameView,
    ReturnGameView,
    OngoingLoansView,
    DetailLoanView,
)

app_name = "inventory"

urlpatterns = [
    path("", InventoryView.as_view(), name="inventory"),
    path("category/", CategoryListView.as_view(), name="category_list"),
    path("category/<slug>/", CategoryView.as_view(), name="category"),
    path("tag/", TagListView.as_view(), name="tag_list"),
    path("tag/<slug>/", TagView.as_view(), name="tag"),
    path("game/", GameListView.as_view(), name="game_list"),
    path("game/<slug>/", GameView.as_view(), name="game"),
    path(
        "game/<slug>/add_comment", AddGameCommentView.as_view(), name="add_game_comment"
    ),
    path(
        "game/<slug>/modify_comment/<int:comment_id>",
        ModifyGameCommentView.as_view(),
        name="modify_game_comment",
    ),
    path("search/", InventorySearchView.as_view(), name="search"),
    path("loans/game/<slug>/", GameLoanView.as_view(), name="game_loan"),
    path("loans/return/<slug>/", ReturnGameView.as_view(), name="return_game"),
    path("loans/borrow/<slug>/", BorrowGameView.as_view(), name="borrow_game"),
    path("loans/ongoing/", OngoingLoansView.as_view(), name="ongoing_loans"),
    path("loans/all/", DetailLoanView.as_view(), name="all_loans"),
]
