from django.contrib import admin
from .models import Suggestion, SuggestionComment
from comments.admin import CommentAdmin


class SuggestionAdmin(admin.ModelAdmin):
    exclude = ("upvoting_users",)
    list_display = ("title", "num_upvotes", "price")
    actions = ["reset_upvotes"]

    def num_upvotes(self, obj):
        return obj.upvoting_users.all().count()

    num_upvotes.short_description = "Nombre de votes"

    def reset_upvotes(self, request, queryset):
        SuggestionVote = Suggestion.upvoting_users.through
        SuggestionVote.objects.filter(suggestion__in=queryset).delete()
        self.message_user(
            request,
            "Les votes pour {} suggestions ont été réinitialisés".format(
                queryset.count()
            ),
        )

    reset_upvotes.short_description = (
        "Remettre à zero les votes pour les suggestions sélectionnées"
    )


admin.site.register(Suggestion, SuggestionAdmin)
admin.site.register(SuggestionComment, CommentAdmin)
