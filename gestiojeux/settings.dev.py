"""
Django settings for jeulee project — dev base settings
"""

import os
from .settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "CHANGE_ME"  # FIXME

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

# Search engine
# https://django-haystack.readthedocs.io/en/latest/tutorial.html#configuration

HAYSTACK_CONNECTIONS = {
    "default": {
        "ENGINE": "haystack.backends.whoosh_backend.WhooshEngine",
        "PATH": os.path.join(BASE_DIR, "whoosh_index"),
    },
}

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Directories
STATIC_ROOT = os.path.join(PUBLIC_DIR, "static")
MEDIA_ROOT = os.path.join(PUBLIC_DIR, "media")

# Email
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
