from django_cas_ng.backends import CASBackend as DjangoCasNgBackend


class CasBackend(DjangoCasNgBackend):
    def clean_username(self, username):
        # We need to build an email out of the CAS username
        return username.lower() + "@clipper.ens.fr"
