from django.contrib import admin
from .models import Category, Tag, Game, GameComment
from comments.admin import CommentAdmin

admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Game)
admin.site.register(GameComment, CommentAdmin)
