from haystack import indexes
from .models import Category, Tag, Game


class CategoryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr="name")

    def get_model(self):
        return Category


class TagIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr="name")

    def get_model(self):
        return Tag


class GameIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(
        document=True,
        use_template=True,
        template_name="inventory/search_indexes/game.txt",
    )

    def get_model(self):
        return Game
