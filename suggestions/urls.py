from django.urls import path
from .views import (
    SuggestionListView,
    AddSuggestionView,
    SuggestionView,
    UpvoteSuggestionView,
    DownvoteSuggestionView,
    AddSuggestionCommentView,
    ModifySuggestionCommentView,
)

app_name = "suggestions"

urlpatterns = [
    path("", SuggestionListView.as_view(), name="suggestions"),
    path("add/", AddSuggestionView.as_view(), name="add_suggestion"),
    path("item/<slug>/", SuggestionView.as_view(), name="suggestion"),
    path(
        "item/<slug>/upvote", UpvoteSuggestionView.as_view(), name="upvote_suggestion"
    ),
    path(
        "item/<slug>/downvote",
        DownvoteSuggestionView.as_view(),
        name="downvote_suggestion",
    ),
    path(
        "item/<slug>/add_comment",
        AddSuggestionCommentView.as_view(),
        name="add_suggestion_comment",
    ),
    path(
        "item/<slug>/modify_comment/<int:comment_id>",
        ModifySuggestionCommentView.as_view(),
        name="modify_suggestion_comment",
    ),
]
