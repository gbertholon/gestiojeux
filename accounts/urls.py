from django.urls import include, path
import django.contrib.auth.views as dj_auth_views
from .views import LoginView, LogoutView, PasswordChangeView, AccountSettingsView
import django_cas_ng.views

app_name = "accounts"

cas_patterns = [
    path("login/", django_cas_ng.views.LoginView.as_view(), name="cas_ng_login"),
    path("logout/", django_cas_ng.views.LogoutView.as_view(), name="cas_ng_logout"),
    path(
        "callback/",
        django_cas_ng.views.CallbackView.as_view(),
        name="cas_ng_proxy_callback",
    ),
]

accounts_patterns = [
    path("cas/", include(cas_patterns)),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("password_login/", dj_auth_views.LoginView.as_view(), name="password_login"),
    path("change_password/", PasswordChangeView.as_view(), name="change_password"),
    path("settings/", AccountSettingsView.as_view(), name="account_settings"),
]

urlpatterns = [
    path("", include(accounts_patterns)),
]
