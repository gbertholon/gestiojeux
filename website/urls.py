from django.urls import path
from .views import MarkdownPageView

app_name = "website"

urlpatterns = [
    path("", MarkdownPageView.as_view(), {"slug": ""}, name="home"),
    path("<slug:slug>/", MarkdownPageView.as_view(), name="md_page"),
]
