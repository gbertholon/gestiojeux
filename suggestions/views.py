from django.views.generic import ListView, DetailView, FormView, RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.shortcuts import redirect
from django.db.models import Count
from comments.views import AddCommentView, ModifyCommentView
from .models import Suggestion, SuggestionComment
from .forms import AddSuggestionForm


class SuggestionListView(ListView):
    model = Suggestion
    template_name = "suggestions/suggestion_list.html"

    def get_queryset(self):
        return Suggestion.objects.annotate(
            num_upvotes=Count("upvoting_users")
        ).order_by("-num_upvotes")


class AddSuggestionView(LoginRequiredMixin, FormView):
    template_name = "suggestions/add_suggestion.html"
    form_class = AddSuggestionForm

    def form_valid(self, form):
        suggestion = form.save()
        suggestion.upvoting_users.add(self.request.user)
        suggestion.comments.create(
            author=self.request.user, text=form.cleaned_data["comment"]
        )
        messages.success(self.request, "Votre suggestion est enregistrée")
        return redirect("suggestions:suggestion", suggestion.slug)


class SuggestionView(DetailView):
    model = Suggestion
    template_name = "suggestions/suggestion.html"


class UpvoteSuggestionView(LoginRequiredMixin, SingleObjectMixin, RedirectView):
    model = Suggestion
    pattern_name = "suggestions:suggestion"

    def get_redirect_url(self, *args, **kwargs):
        suggestion = self.get_object()
        if self.request.user not in suggestion.upvoting_users.all():
            suggestion.upvoting_users.add(self.request.user)
            messages.success(
                self.request, "Votre vote pour cette suggestion est enregistré"
            )
        else:
            messages.error(self.request, "Vous avez déjà voté pour cette suggestion")

        return super().get_redirect_url(*args, **kwargs)


class DownvoteSuggestionView(LoginRequiredMixin, SingleObjectMixin, RedirectView):
    model = Suggestion
    pattern_name = "suggestions:suggestion"

    def get_redirect_url(self, *args, **kwargs):
        suggestion = self.get_object()
        if self.request.user in suggestion.upvoting_users.all():
            suggestion.upvoting_users.remove(self.request.user)
            messages.success(
                self.request, "Votre vote pour cette suggestion a été retiré"
            )
        else:
            messages.error(
                self.request, "Vous ne votiez déjà pas pour cette suggestion"
            )

        return super().get_redirect_url(*args, **kwargs)


class AddSuggestionCommentView(AddCommentView):
    model = Suggestion
    comment_model = SuggestionComment
    pattern_name = "suggestions:suggestion"


class ModifySuggestionCommentView(ModifyCommentView):
    model = Suggestion
    comment_model = SuggestionComment
    template_name = "suggestions/suggestion.html"
    success_pattern_name = "suggestions:suggestion"
