from django.db import models
from autoslug import AutoSlugField
from django.utils.timezone import now

class AbstractLoan(models.Model):
    lent_object = None # Fill this with a foreign key in subclasses
    slug = AutoSlugField(unique=True, populate_from="lent_object")
    borrow_date = models.DateTimeField(
        auto_now_add=True, verbose_name="Date d’emprunt")
    return_date = models.DateTimeField(null=True, verbose_name="Date de retour")
    mail = models.EmailField()

    lent_object_slug_field = "slug"

    class Meta:
        abstract = True
        ordering=["borrow_date"]
        verbose_name = "emprunt"
        verbose_name_plural = "emprunts"

    def __str__(self):
        return self.slug

    def return_object(self):
        self.return_date = now()
        self.save()

    @classmethod
    def ongoing_loans(cls, obj = None):
        ongoing = cls.objects.filter(return_date=None)
        if obj != None:
            return ongoing.filter(lent_object=obj)
        else:
            return ongoing
