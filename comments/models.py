from django.db import models
from accounts.models import User


class AbstractComment(models.Model):
    commented_object = None  # Fill this with a foreign key in subclasses
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="auteur·ice"
    )
    text = models.TextField(verbose_name="texte")
    created_on = models.DateTimeField(
        auto_now_add=True, verbose_name="date de publication"
    )
    modified_on = models.DateTimeField(
        auto_now=True, verbose_name="date de modification"
    )

    class Meta:
        abstract = True
        ordering = ["created_on"]

    def __str__(self):
        return "({}) {}: {}".format(self.commented_object, self.author, self.text)
